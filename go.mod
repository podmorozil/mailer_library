module gitlab.com/podmorozil/mailer_library

go 1.17

require (
	github.com/hoisie/mustache v0.0.0-20160804235033-6375acf62c69
	github.com/streadway/amqp v1.0.0
	go.uber.org/zap v1.21.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
