package mailer_library

import (
	"fmt"
	"os"
)

func LoadAMQPSettings() (host string, port string, user string, password string) {
	host = os.Getenv("AMQP_HOST")
	port = os.Getenv("AMQP_PORT")
	user = os.Getenv("AMQP_USER")
	password = os.Getenv("AMQP_PASSWORD")

	if host == "" {
		host = "127.0.0.1"
	}

	if port == "" {
		port = "5672"
	}

	if user == "" {
		user = "guest"
	}

	if password == "" {
		password = "guest"
	}

	return
}

func LoadIsDebug() bool {
	debug := os.Getenv("DEBUG")
	
	if debug == "" {
		return false
	}

	return debug == "1"
}

func LoadInstanceId() (id string) {
	id = os.Getenv("CONTAINER_ID")
	
	if id == "" {
		return "Unknown"
	}

	return
}

func LoadProbePort() (port string) {
	port = os.Getenv("PROBE_PORT")
	
	if port == "" {
		return "8090"
	}

	return
}

func LoadTokens() []string {
	var tokens []string

	for i := 0; ; i++ {
		token := os.Getenv(fmt.Sprintf("TOKEN_%d", i))
		if token == "" {
			break
		}

		tokens = append(tokens, token)
	}

	return tokens
}
