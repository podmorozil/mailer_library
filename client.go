package mailer_library

import (
	"fmt"

	"github.com/streadway/amqp"
)

type Client struct {
	connection *amqp.Connection
	channel    *amqp.Channel
}

func NewClient() *Client {
	return &Client{}
}

func (client *Client) Start() error {
	host, port, user, password := LoadAMQPSettings()

	connection, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s", user, password, host, port))
	if err != nil {
		return err
	}

	channel, err := connection.Channel()
	if err != nil {
		return err
	}

	client.connection = connection
	client.channel = channel
	return nil
}

func (client *Client) Stop() {
	client.channel.Close()
	client.connection.Close()
}

func (client *Client) Register(senderNames []string) error {
	exchangeName := "mailer"

	err := client.channel.ExchangeDeclare(
		exchangeName,
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	for _, senderName := range senderNames {
		_, err = client.channel.QueueDeclare(
			senderName,
			false,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			return err
		}
	}

	return nil
}

func (client *Client) SendMessage(senderName string, channelId uint, recipientId string, templateId string, message string) error {
	return client.channel.Publish(
		"mailer",
		FormatRoutingKey(senderName, channelId, recipientId, templateId),
		false,
		false,
		amqp.Publishing{
			Body: []byte(message),
		},
	)
}
