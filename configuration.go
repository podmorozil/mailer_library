package mailer_library

import (
	"errors"
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

type Template struct {
	Id      string `yaml:"id"`
	Content string `yaml:"content"`
}

type Configuration struct {
	Templates []Template `yaml:"templates"`
}

type TemplatesMap map[string]string

func LoadConfiguration() (*Configuration, error) {
	var configuration Configuration
	var path string

	if isFileExist("/etc/mailer/configuration.yaml") {
		path = "/etc/mailer/configuration.yaml"
	} else if isFileExist("configuration.yaml") {
		path = "configuration.yaml"
	} else {
		return &configuration, nil
	}

	file, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(file, &configuration)
	if err != nil {
		return nil, err
	}

	return &configuration, nil
}

func BuildTemplates(configuration *Configuration) TemplatesMap {
	var templates = make(TemplatesMap)

	for _, template := range configuration.Templates {
		templates[template.Id] = template.Content
	}

	return templates
}

func isFileExist(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}
