package mailer_library

import (
	"context"
	"fmt"
	"net/http"
	"sync"
)

type ProbeResponder struct {
	context    context.Context
	httpServer *http.Server
	isReady    bool
	lock       sync.Mutex
	waitGroup  sync.WaitGroup
}

func NewProbeResponder(context context.Context, port string) *ProbeResponder {
	probeResponder := ProbeResponder{
		context: context,
		httpServer: &http.Server{
			Addr: fmt.Sprintf(":%s", port),
		},
	}
	http.HandleFunc("/v0/probe/startup", probeResponder.probeHandler)
	http.HandleFunc("/v0/probe/liveness", probeResponder.probeHandler)
	http.HandleFunc("/v0/probe/readiness", probeResponder.probeHandler)

	return &probeResponder
}

func (probeResponder *ProbeResponder) Start() {
	go probeResponder.loop()
}

func (probeResponder *ProbeResponder) Stop() {
	probeResponder.httpServer.Shutdown(probeResponder.context)
	probeResponder.waitGroup.Wait()
}

func (probeResponder *ProbeResponder) GetIsReady() bool {
	probeResponder.lock.Lock()
	defer probeResponder.lock.Unlock()

	return probeResponder.isReady
}

func (probeResponder *ProbeResponder) SetIsReady(value bool) {
	probeResponder.lock.Lock()
	defer probeResponder.lock.Unlock()

	probeResponder.isReady = value
}

func (probeResponder *ProbeResponder) loop() {
	probeResponder.waitGroup.Add(1)
	defer probeResponder.waitGroup.Done()

	err := probeResponder.httpServer.ListenAndServe()
	if err != nil {
		// TODO: use logger
		fmt.Println(err)
		return
	}
}

func (probeResponder *ProbeResponder) probeHandler(response http.ResponseWriter, request *http.Request) {
	if !probeResponder.GetIsReady() {
		response.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	response.WriteHeader(http.StatusOK)
}
