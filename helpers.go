package mailer_library

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

func ParseRoutingKey(key string) (channelId uint, recipientId string, templateId string, err error) {
	parts := strings.Split(key, ".")
	if len(parts) != 4 {
		err = errors.New("")
		return
	}

	channelId, err = Atoi(parts[1])
	if err != nil {
		return
	}

	recipientId = parts[2]
	templateId = parts[3]
	return
}

func FormatRoutingKey(senderName string, channelId uint, recipientId string, templateId string) string {
	return fmt.Sprintf("%s.%d.%s.%s", senderName, channelId, recipientId, templateId)
}

func Atoi(valueString string) (uint, error) {
	valueInt, err := strconv.Atoi(valueString)
	if err != nil {
		return 0, err
	}

	if valueInt < 0 {
		return 0, errors.New("value less than zero")
	}

	return uint(valueInt), nil
}
