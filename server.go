package mailer_library

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"

	"github.com/hoisie/mustache"
	"github.com/streadway/amqp"
	"go.uber.org/zap"
)

type MessageHandler func(channelId uint, recipientId string, text string) error

type Server struct {
	context        context.Context
	log            *zap.SugaredLogger
	waitGroup      sync.WaitGroup
	Name           string
	IsDebug        bool
	InstanceId     string
	Configuration  *Configuration
	Templates      TemplatesMap
	ProbeResponder *ProbeResponder
	Connection     *amqp.Connection
	Channel        *amqp.Channel
	Queue          *amqp.Queue
	OnMessage      <-chan amqp.Delivery
}

func NewServer(context context.Context, log *zap.SugaredLogger, name string) *Server {
	return &Server{
		context: context,
		log:     log,
		Name:    name,
	}
}

func (server *Server) Start(handler MessageHandler) error {
	// Instance
	server.IsDebug = LoadIsDebug()
	server.InstanceId = LoadInstanceId()

	// Templates
	configuration, err := LoadConfiguration()
	if err != nil {
		return err
	}

	server.Configuration = configuration
	server.Templates = BuildTemplates(configuration)

	// Probe responder
	port := LoadProbePort()
	server.ProbeResponder = NewProbeResponder(server.context, port)
	server.ProbeResponder.Start()

	// RabbitMQ
	exchangeName := "mailer"
	routingKey := fmt.Sprintf("%s.*.*.*", server.Name)
	queueName := server.Name

	host, port, user, password := LoadAMQPSettings()

	connection, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s", user, password, host, port))
	if err != nil {
		return err
	}

	channel, err := connection.Channel()
	if err != nil {
		return err
	}

	err = channel.ExchangeDeclare(
		exchangeName,
		"topic",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	err = channel.QueueBind(
		queueName,
		routingKey,
		exchangeName,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	queue, err := channel.QueueDeclare(
		queueName,
		false,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	messages, err := channel.Consume(
		queueName,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	server.Connection = connection
	server.Channel = channel
	server.Queue = &queue
	server.OnMessage = messages

	go server.loop(handler)
	return nil
}

func (server *Server) Stop() {
	server.Channel.Close()
	server.Connection.Close()
	server.ProbeResponder.Stop()
	server.waitGroup.Wait()
}

func (server *Server) loop(handler MessageHandler) {
	server.waitGroup.Add(1)

	go func() {
		server.ProbeResponder.SetIsReady(true)

		for {
			select {
			case message := <-server.OnMessage:
				if server.IsDebug {
					server.log.Debug("Got message")
				}

				channelId, recipientId, templateId, err := ParseRoutingKey(message.RoutingKey)
				if err != nil {
					server.log.Errorw("Error parse routing key", "routing_key", message.RoutingKey)
					continue
				}

				template, ok := server.Templates[templateId]
				if !ok {
					server.log.Errorw("Template not found", "template_id", templateId)
					continue
				}

				var parameters map[string]interface{}
				err = json.Unmarshal(message.Body, &parameters)
				if err != nil {
					server.log.Errorw("Params unmarshal failed", "parameters", string(message.Body))
					continue
				}

				suffix := ""
				if server.IsDebug {
					suffix += fmt.Sprintf("\ninstance_id: %s", server.InstanceId)
				}

				err = handler(
					channelId,
					recipientId,
					mustache.Render(
						template,
						parameters,
					)+suffix,
				)
				if err != nil {
					server.log.Errorw("Error handling message", "error", err)
					continue
				}

				// TODO: ack
			case <-server.context.Done():
				server.ProbeResponder.SetIsReady(false)
				server.waitGroup.Done()
				return
			}
		}
	}()
}
